# [youtube-dl][project]

[![License][license_md]][license]
[![GitLab CI][gitlab_ci]][gitlab]
[![Docker Pull][docker_pull]][docker]
[![Docker Star][docker_star]][docker]
[![Docker Size][docker_size]][docker]
[![Docker Layer][docker_layer]][docker]

[youtube-dl][ydl] with FFmpeg in Alpine.

This image is built at least once a week with latest version of youtube-dl.

## Usage

```bash
docker run -v "$(pwd):/ydl" --rm -d joshava/youtube-dl --version
```

Please refer to [youtube-dl][ydl] readme for references.

[docker]: https://hub.docker.com/r/joshava/youtube-dl
[docker_pull]: https://img.shields.io/docker/pulls/joshava/youtube-dl.svg
[docker_star]: https://img.shields.io/docker/stars/joshava/youtube-dl.svg
[docker_size]: https://img.shields.io/microbadger/image-size/joshava/youtube-dl.svg
[docker_layer]: https://img.shields.io/microbadger/layers/joshava/youtube-dl.svg
[license]: https://gitlab.com/joshua-avalon/docker/youtube-dl/blob/master/LICENSE
[license_md]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[gitlab]: https://gitlab.com/joshua-avalon/docker/youtube-dl/pipelines
[gitlab_ci]: https://gitlab.com/joshua-avalon/docker/youtube-dl/badges/master/pipeline.svg
[ydl]: https://github.com/ytdl-org/youtube-dl
[project]: https://gitlab.com/joshua-avalon/docker/youtube-dl
