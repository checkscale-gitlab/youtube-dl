FROM python:3-alpine

ARG VERSION

WORKDIR /ydl

RUN apk add --update --no-cache ffmpeg && \
    pip install youtube_dl==$VERSION

ENTRYPOINT ["youtube-dl"]
CMD ["--version"]
